#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int d(int n) // Dice function, returns a random number between 1 and n - its int argument; for example, d(6) means d6
{
    int num=n;
    if(n<0) num=-n;
    else if(n==0) num=1;
    return 1+rand()%num;
}

int checavogal(char c) // Boolean function, returns true if char argument is a vowel
{
    if((c=='a')||(c=='e')||(c=='i')||(c=='o')||(c=='u')||(c=='A')||(c=='E')||(c=='I')||(c=='O')||(c=='U')) return 1;
    return 0;
}

/*
int checaoutros(char c) // Boolean function, returns true if char argument is either ['], [-] or an empty space ([ ]) 
{
    if((c==' ')||(c=='\'')||(c=='-')) return 1;
    return 0;
}
*/

int main(){

    char nome[13];
    char vogais[10]={'a', 'e', 'o', 'i', 'u', 'A', 'E', 'O', 'I', 'U'};
    char consoantes[42]={'k', 'w', 'j', 'f', 'b', 'g', 'l', 'c', 'm', 'd', 's', 'r', 'n', 't', 'p', 'v', 'q', 'h', 'z', 'x', 'y', 'K', 'W', 'J', 'F', 'B', 'G', 'L', 'C', 'M', 'D', 'S', 'R', 'N', 'T', 'P', 'V', 'Q', 'H', 'Z', 'X', 'Y'};
    int i,j,k,min1,min2,nome_tam,dice_num,seq_vogal,aleat,continua,acumulado,posit,tents,max_tents=10;
    
    // Start RNG
    srand(time(0));
    
    // Provide a hundred names
    for(i=0;i<100;i++){
        
        // Roll 1d6+4 for the name's length
        // The written algorithm uses 2d6; this change is strictly for personal reasons
        nome_tam=4;
        // dice_num=1;
        // for (j=0;j<dice_num;j++)
         nome_tam+=d(6);

        // Start name generation
        for(j=0;j<nome_tam;j++){
            
            // Roll 1d6 for vowel or consonant
            aleat=d(6);
            
            // Check previous letters to break consecutive sequences of vowels or consonants
            if(j>0){
                // if(!checaoutros(nome[j-1])){
                    if(checavogal(nome[j-1])) seq_vogal=1;
                    else seq_vogal=0;
                    continua=1;
                    acumulado=1;
                    for(k=j-1;(k>=0)&&(continua);k--){
                        // if(checaoutros(nome[k])) continua=0;
                        // else{
                            if(seq_vogal!=checavogal(nome[k])) continua=0;
                            else acumulado++;
                        // }
                    }
                    if(seq_vogal) aleat+=acumulado;
                    else aleat-=acumulado;
                // }
            }
            
            // If vowel, roll d6 twice and pick the smallest value
            if(aleat<4){
                
                // Reroll if 6
                tents=1;
                do{
                    min1=d(6);
                    min2=d(6);
                    if(min1<min2) posit=min1;
                    else posit=min2;
                    if(posit==6) tents++;
                }while((posit==6)&&(tents<max_tents));
                if(tents==max_tents) posit=1;
                
                posit-=1;
                if(j==0) posit+=5;
                else if(nome[j-1]==' ') posit+=5;
                nome[j]=vogais[posit];

            }
            
            // If consonant, roll 4d6
            else{
                
                aleat=0;
                dice_num=4;
                for (k=0;k<dice_num;k++) aleat+=d(6);
                
                posit=aleat-4;
                if(j==0) posit+=21;
                else if(nome[j-1]==' ') posit+=21;
                nome[j]=consoantes[posit];
                
            }
            
            /*
            // After letter, roll 2d6 (except last letter)
            if(j<nome_tam-1){
                
                aleat=0;
                dice_num=2;
                for (k=0;k<dice_num;k++) aleat+=d(6);
                
                // If 2 or 12, roll 1d6
                if((aleat==2)||(aleat==12)){
                    k=d(6);
                    j++;
                    if((k==1)||(k==2)) nome[j]='\'';
                    else if((k==3)||(k==4)) nome[j]='-';
                    else nome[j]=' ';
                    nome_tam++;
                }
            }
            */
            
        }
        
        // Print name
        for(j=0;j<nome_tam;j++) printf("%c",nome[j]);
        printf("\n");
    }

    return 0;
}

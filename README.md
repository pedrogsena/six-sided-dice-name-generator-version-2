# Six-sided Dice Name Generator, Version 2

A simple random name generator using common, six-sided dice. Useful for generating possible ficcional names for fantasy worlds and places, or sci-fi planets and stars. Vowels and consonants are ordered based on their letter frequency in the Portuguese language, as seen [here](https://en.wikipedia.org/wiki/Letter_frequency "Letter Frequency - Wikipedia").

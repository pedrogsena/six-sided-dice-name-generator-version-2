# Algorithm

1. Roll 2d6 to find out how many letters the name will have at the end.
2. Starting from the first, for each letter: roll 1d6.
    1. If the previous character was a vowel, for it and for each consecutive vowel before it add 1 to the roll.
    2. Else, if it was a consonant, for it and for each consecutive consonant before it subtract 1 from the roll.
3. If you rolled 4+ it's a consonant, else it's a vowel.
4. If it's a vowel roll 1d6 twice and pick the smallest value.
    1. If it was 1, 2, 3, 4, 5 then the vowel is [a], [e], [o], [i], [u] respectively.
    2. Else roll again.
5. If it's a consonant, roll 4d6 and check on the Consonant Table below.
<!---6. Now roll 2d6. (Don't do this for the last letter.) If you rolled 2 or 12, roll another d6. Then, after the letter, add ['] if you rolled 1 or 2, [-] if you rolled 3 or 4, or an empty space [ ] if you rolled 5 or 6.-->
6. Repeat 2–5 until you fill the entire name. If you didn't like it, throw it away and start anew.

**Consonant Table:**

| Value | Letter | Value | Letter | Value | Letter |
| ----- | ------ | ----- | ------ | ----- | ------ |
|   4   |   K    |   11  |   C    |  18   |   P    |
|   5   |   W    |   12  |   M    |  19   |   V    |
|   6   |   J    |   13  |   D    |  20   |   Q    |
|   7   |   F    |   14  |   S    |  21   |   H    |
|   8   |   B    |   15  |   R    |  22   |   Z    |
|   9   |   G    |   16  |   N    |  23   |   X    |
|   10  |   L    |   17  |   T    |  24   |   Y    |
